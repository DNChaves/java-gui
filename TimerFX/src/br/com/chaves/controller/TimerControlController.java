package br.com.chaves.controller;

import java.net.URL;
import java.util.ResourceBundle;

import br.com.chaves.main.TimerFXNavigator;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class TimerControlController implements Initializable {

	@FXML
	private Label tempoLbl;
	@FXML
	private TextField tempoTxt;
	@FXML
	private Button startBtn;
	
	private TimerSingleton timerControl;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		startBtn.disableProperty().bind(
				Bindings.createBooleanBinding(
						() -> tempoTxt.getText().length() == 0,
						tempoTxt.textProperty()
						)
				);
		
		startBtn.setOnAction(event -> iniciarTimer());
		
	}

	private Object iniciarTimer() {	
				
		timerControl = TimerSingleton.getInstance();
		Integer timerValue;
		try {
			timerValue = Integer.parseInt(tempoTxt.getText());
		} catch (NumberFormatException e) {			
			Alert alert = new Alert(AlertType.ERROR, "Valor incorreto. (Requerido numero inteiro)", ButtonType.OK);
			alert.show();
			return null;
		}
		System.out.println("Iniciar Timer...");
		timerControl.setTotalTime(timerValue);
		timerControl.start();
		
		TimerFXNavigator.loadVista(TimerFXNavigator.RUNNING_PANE);
		
		return null;
	}

	

	
}
