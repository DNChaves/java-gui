package br.com.chaves.controller;

import java.net.URL;
import java.util.ResourceBundle;

import br.com.chaves.main.TimerFXNavigator;
import javafx.beans.binding.Bindings;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

public class TimerRunningController implements Initializable {

	@FXML
	private Label progressoLbl;
	@FXML
	private ProgressBar progressoPBar;
	@FXML
	private Button stopBtn;
	@FXML
	private Button pauseResumeBtn;
	
	private TimerSingleton timerControl = TimerSingleton.getInstance();
		
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		stopBtn.setOnAction(event -> pararTimer());
						
		pauseResumeBtn.setOnAction(event -> pauseResumeTimer());
		
		threadUpdateProgress = new Thread(task);
		threadUpdateProgress.start();
		progressoPBar.progressProperty().bind(task.progressProperty());
		
		stopBtn.textProperty().bind(Bindings.createStringBinding(() -> timerControl.isRunning() ? "Stop!" : "Done", task.progressProperty()));
	}

	private Object pauseResumeTimer() {
		
		//timerControl = TimerSingleton.getInstance();
		
		timerControl.pauseResume();
		if (timerControl.isPaused()) {
			pauseResumeBtn.setText("Resume");
		} else {
			pauseResumeBtn.setText("Pause");
		}
				
		return null;
	}

	private Object pararTimer() {
		//timerControl = TimerSingleton.getInstance();
		timerControl.stop();		
		threadUpdateProgress.interrupt();
		
		TimerFXNavigator.loadVista(TimerFXNavigator.CONTROL_PANE);
		return null;
	}
	
	private Thread threadUpdateProgress;

	private Task<Void> task = new Task<Void>() {

	     @Override protected Void call() throws Exception {
 
	        timerControl = TimerSingleton.getInstance();
	 			 		
	        while (timerControl.isRunning()) {			
				updateProgress(timerControl.getProgress(), 1);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
 			return null;	 		
	     }
	 };
		
}
