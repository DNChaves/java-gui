package br.com.chaves.controller;

import java.awt.Toolkit;

public class TimerSingleton {
	
	private static TimerSingleton instance;
	
	protected TimerSingleton() {}
	
	public static TimerSingleton getInstance() {
		if (instance == null) {
			instance = new TimerSingleton();
		}
		return instance;
	}
	
	private Integer totalTime;
	
	public Integer getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(Integer totalTime) {
		this.totalTime = totalTime;
	}

	private Long currentTime;
	
	private Long endTime;
	
	private Long remainingTime;
	
	private Thread crono;
	
	private Thread beep;

	private boolean paused = false;

	private Long initTime;
	
	private void initTimer() {		
		currentTime = 0l;
		initTime = System.currentTimeMillis();
		endTime = initTime + (totalTime * 1000 * 60);
		crono = new Thread(() -> {
			try {
				while (currentTime <= endTime) {
					Thread.sleep(500);
					if (!paused) {
						currentTime = System.currentTimeMillis();
						remainingTime = endTime - currentTime;
					}				
				}
				finishTimer();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
		});
	}
	
	private void finishTimer() {
		System.out.println("Timer encerrado!");
		beep = new Thread(() -> {
			try {
				while(true) {
					Toolkit.getDefaultToolkit().beep();					
					Thread.sleep(1000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		beep.start();
		crono.interrupt();
	}

	public void reset() {
		
		
	}
	
	public void start() {
		initTimer();
		crono.start();
	}
	
	public synchronized void pauseResume() {
		paused = !paused;
		if (!paused) {
			endTime = System.currentTimeMillis() + remainingTime;
		}
	}
	
	public boolean isPaused() {
		return paused;
	}

	public void setPaused(boolean paused) {
		this.paused = paused;
	}

	public void stop() {
		crono.interrupt();
		if (beep != null && beep.isAlive()) {
			beep.interrupt();
		}
		paused = false;
	}
	
	public boolean isRunning() {
		return crono != null ? crono.isAlive() : false;
	}

	public Double getProgress() {
		Double result = 0.00d;
		
		if(initTime != null) {
			result = ((currentTime - initTime)* 1.0000d) / (endTime - initTime);
		}
		
		return result;
	}

	

}
