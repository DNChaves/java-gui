package br.com.chaves.navigation.controller;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;

public class NavigationController {

    @FXML
    private StackPane vistaHolder;


    public void setVista(Node node) {
        vistaHolder.getChildren().setAll(node);        
    }

}
