package br.com.chaves.main;

import java.io.IOException;

import br.com.chaves.navigation.controller.NavigationController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

public class TimerFXNavigator {
	
	public static final String MAIN = "main.fxml";
    public static final String CONTROL_PANE = "control.fxml";
    public static final String RUNNING_PANE = "running.fxml";
    
    private static NavigationController navigationController;

    
    public static void setNavigationController(NavigationController navigationController) {
        TimerFXNavigator.navigationController = navigationController;
    }

    
    public static void loadVista(String fxml) {
        try {
        	navigationController.setVista(
                (Node)FXMLLoader.load(
                    TimerFXNavigator.class.getResource(
                        fxml
                    )
                )
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    

}
