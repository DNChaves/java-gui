package br.com.chaves.main;

import java.io.IOException;

import br.com.chaves.navigation.controller.NavigationController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class TimerFX extends Application {

	public static void main(String[] args) {
		launch(args);
		
	}
	
	@Override
	public void start(Stage stage) throws Exception {
	
		stage.setTitle("Timer FX By DNChaves");
		stage.setScene(createScene(
                	loadMainPane()
				));	
		
		stage.show();

	}
	
	private Pane loadMainPane() throws IOException {
        FXMLLoader loader = new FXMLLoader();

        Pane mainPane = (Pane) loader.load(
                getClass().getResourceAsStream(
                    TimerFXNavigator.MAIN
                )
        );

        NavigationController navigationController = loader.getController();

        TimerFXNavigator.setNavigationController(navigationController);
        TimerFXNavigator.loadVista(TimerFXNavigator.CONTROL_PANE);

        return mainPane;
    }
   
    private Scene createScene(Pane mainPane) {
        Scene scene = new Scene(
            mainPane
        );

        return scene;
}

}
