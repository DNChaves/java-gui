package br.com.dnchaves;

import java.awt.Toolkit;

import javax.swing.SwingUtilities;

import br.com.dnchaves.gui.MainFrame;

public class StartApp {

public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				MainFrame frame = new MainFrame();
				frame.init();
				frame.setBounds((Toolkit.getDefaultToolkit().getScreenSize().width / 2) - (frame.getWidth() / 2), 
						(Toolkit.getDefaultToolkit().getScreenSize().height / 2) - (frame.getHeight() / 2), frame.getWidth(), frame.getHeight());
			}
		});
		
	}

}
