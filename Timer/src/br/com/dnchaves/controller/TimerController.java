package br.com.dnchaves.controller;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.Timer;

import br.com.dnchaves.model.TimerModel;

public class TimerController {
	
	TimerModel model;
	
	Timer timer;
	Timer beepTimer;
	
	Boolean paused = false;
	
	private static final String ALERT_FILE_PATH = "/resources/AlienAlert.wav";
	
	public TimerController(TimerModel model) {
		super();
		this.model = model;
		init();
	}
				
	private void init() {
		timer = new Timer(1000, timerListener);
				
		beepTimer = new Timer("sound".equals(model.getSoundType()) ? 10000 : 1000, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if ("sound".equals(model.getSoundType())) {
					playSound();
				} else {
					Toolkit.getDefaultToolkit().beep();
				}
				
			}
			
			private void playSound() {
				try {
					URL defaultSound = getClass().getResource(ALERT_FILE_PATH);
					//InputStream is= getClass().getResourceAsStream(ALERT_FILE_PATH);
					System.out.println("Default sound path: " + defaultSound);
					AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(defaultSound);
					Clip clip = AudioSystem.getClip();
				    clip.open(audioInputStream);
					clip.start();
				} catch (Exception ex) {
					System.out.println("Error with playing sound.");
					ex.printStackTrace();
				}
			}
		});
		
	}
	
	ActionListener timerListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {	
			if (paused) {
				System.out.println("Timer paused");
				return;
			}
			System.out.println("Timer running: " + model.getActualValueSeconds());
			if ((model.getActualValueSeconds() + (beepTimerGap())) >= (model.getTimer() * 60)) {
				play();
			}
			if (model.getActualValue() >= model.getTimer()) {
				System.out.println("Finished");
				stop();
				return;
			}
			model.setActualValueSeconds(model.getActualValueSeconds() + 1);			
			if (model.getCountSeconds() == 59) {
				model.setCountSeconds(0);
				model.setActualValue(model.getActualValue() + 1);
			} else {
				model.setCountSeconds(model.getCountSeconds() + 1);
			}
		}
		
		//Workaround to anticipate the initial delay of Timer class; 
		private Integer beepTimerGap() {
			if ("sound".equals(model.getSoundType())) {
				return 10;
			} else {
				return 1;
			}
			
		}

		private void play() {
			if (!beepTimer.isRunning()) {
				beepTimer.start();
			}
		}
		
	};
	
	public boolean isRunning() {
		
		return timer.isRunning();
	}
	
	public Boolean isPaused() {
		return paused;
	}

	public void stop() {
		timer.stop();		
	}

	public void start() {
		timer.start();
		
	}
	
	public void pauseResume() {
		paused = !paused;
	}

	public void finish() {
		if (beepTimer.isRunning()) {
			beepTimer.stop();
		}		
	}

}
