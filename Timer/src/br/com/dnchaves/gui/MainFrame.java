package br.com.dnchaves.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import br.com.dnchaves.controller.TimerController;
import br.com.dnchaves.model.TimerModel;

public class MainFrame extends JFrame implements PropertyChangeListener, ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2283301593997134722L;
	
	JPanel initPanel;
	JLabel typeSoundLbl;
	ButtonGroup soundTypeGroupBtn;
	JRadioButton beepRadioBtn;
	JRadioButton soundRadioBtn;
	JLabel minutesLabel;
	JTextField minutesTxtField;
	JButton startBtn;
	
	JPanel progressPanel;
	JLabel progressLabel;
	JProgressBar progressBar;
	JButton pauseBtn;
	JButton cancelBtn;
	
	TimerModel model;
	TimerController controller;
	
	UpdateProgressBarTask task;

	
	
	public class UpdateProgressBarTask extends SwingWorker<Void, Void> {
        /*
         * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {
        	setProgress(0);
        	
        	while(controller.isRunning()) {
        		setProgress(calculateProgressPercentual().intValue());
        		try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		//System.out.println("Progress (%): " + getProgress());
        		//System.out.println("Actual value (sec): " + model.getActualValueSeconds());
        	}
        	
        	return null;
        }
        
        @Override
        protected void done() {
        	setProgress(100);
        	super.done();
        }

		private Long calculateProgressPercentual() {
			double calc = (model.getActualValueSeconds() * 100) / (model.getTimer() * 60);
			return Math.round(calc); 
		}
         
    }
	
	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand().equals("start")) {
			if (!isInputValid()) {
				return;
			}
			System.out.println("Timer action start button");
			Container contain = this.getContentPane();
			contain.removeAll();
			buildProgressPanel();
			contain.add(progressPanel);
			contain.validate();
			contain.repaint();
			validate();
			repaint();
			pack();
			setVisible(true);
			setBounds((Toolkit.getDefaultToolkit().getScreenSize().width / 2) - (getWidth() / 2), 
					(Toolkit.getDefaultToolkit().getScreenSize().height / 2) - (getHeight() / 2), getWidth(), getHeight());
			System.out.println("Panel progress changed");
			
			
			model.setMinValue(0);
			model.setCountSeconds(0);
			model.setActualValue(0);
			model.setActualValueSeconds(0);
			Integer timeSetted = Integer.parseInt(minutesTxtField.getText());
			model.setTimer(timeSetted);
			progressBar.setModel(model);
			
			System.out.println("Starting controller");
			controller = new TimerController(model);
			task = new UpdateProgressBarTask();
			task.addPropertyChangeListener(this);
			controller.start();
	        task.execute();
	        
			System.out.println("Controller Started");
		}
		
		if (e.getActionCommand().equals("pause")) {
			if (!controller.isPaused()) {
				pauseBtn.setText("Resume");					
			} else {
				pauseBtn.setText("Pause");
			}
			controller.pauseResume();
		}
		
		if (e.getActionCommand().equals("stop")) {
			if (controller.isRunning()) {
				controller.stop();
				controller.finish();
			}
			model.setActualValue(model.getMinValue());
			model.setCountSeconds(0);
			controller.finish();
			Container contain = this.getContentPane();
			contain.removeAll();
			buildInitPanel();
			contain.add(initPanel);
			contain.validate();
			contain.repaint();
			validate();
			repaint();
			pack();
			setVisible(true);
			setBounds((Toolkit.getDefaultToolkit().getScreenSize().width / 2) - (getWidth() / 2), 
					(Toolkit.getDefaultToolkit().getScreenSize().height / 2) - (getHeight() / 2), getWidth(), getHeight());
		}
		
		if (e.getActionCommand().equals("beep")) {
			model.setSoundType("beep");
		}
		
		if (e.getActionCommand().equals("sound")) {
			model.setSoundType("sound");
		}
						
	}
		
	private boolean isInputValid() {
		String minutesStr = minutesTxtField.getText();
		try {
			int minutes = Integer.parseInt(minutesStr);
			if (minutes < 1) {
				throw new NumberFormatException("Must be greater then 0.");
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(initPanel, "Minutes must be specified and be an Integer greater then 0.", "Error!", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	public void init() {
		this.setTitle("Timer");
		
		model = new TimerModel();
						
		buildInitPanel();
		
		buildProgressPanel();		
		
		setContentPane(initPanel);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
						
		this.pack();
		this.setVisible(true);
	}

	private void buildProgressPanel() {
		progressPanel = new JPanel(new BorderLayout());
		progressLabel = new JLabel("Time: ");
		progressBar = new JProgressBar();
		progressBar.setValue(0);
        progressBar.setStringPainted(true);
		pauseBtn = new JButton("Pause");
		pauseBtn.setActionCommand("pause");
		pauseBtn.addActionListener(this);
		cancelBtn = new JButton("Stop");
		cancelBtn.setActionCommand("stop");
		cancelBtn.addActionListener(this);
		JPanel center = new JPanel();
		center.setLayout(new FlowLayout());
		center.add(progressLabel);
		center.add(progressBar);
		progressPanel.add(center, BorderLayout.CENTER);
		JPanel south = new JPanel();
		south.setLayout(new FlowLayout());
		south.add(pauseBtn);
		south.add(cancelBtn);
		progressPanel.add(south, BorderLayout.SOUTH);
	}

	private void buildInitPanel() {
		initPanel = new JPanel(new FlowLayout());
		typeSoundLbl = new JLabel("On Finish, play: ");
		beepRadioBtn = new JRadioButton("Beep");
		beepRadioBtn.setSelected(true);
		beepRadioBtn.setActionCommand("beep");
		soundRadioBtn = new JRadioButton("Sound");
		soundRadioBtn.setActionCommand("sound");
		soundTypeGroupBtn = new ButtonGroup();
		minutesLabel = new JLabel("Time (min):");
		minutesTxtField = new JTextField(5);
		startBtn = new JButton("Start!");
		startBtn.setActionCommand("start");
		
		beepRadioBtn.addActionListener(this);
		soundRadioBtn.addActionListener(this);
		soundTypeGroupBtn.add(beepRadioBtn);
		soundTypeGroupBtn.add(soundRadioBtn);
		
		startBtn.addActionListener(this);
		initPanel.add(typeSoundLbl);
		initPanel.add(beepRadioBtn);
		initPanel.add(soundRadioBtn);
		initPanel.add(minutesLabel);
		initPanel.add(minutesTxtField);
		initPanel.add(startBtn);
	}
	
	public void propertyChange(PropertyChangeEvent evt) {
		if ("progress".equals(evt.getPropertyName())) {
			//if (controller.isRunning()) {
		        progressBar.setValue(model.getActualValueSeconds());
		        progressLabel.setText(model.getActualValueSeconds() + " s");
		    //}
        }

    }
			
}
