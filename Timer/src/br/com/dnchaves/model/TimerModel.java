package br.com.dnchaves.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.BoundedRangeModel;
import javax.swing.event.ChangeListener;

public class TimerModel implements BoundedRangeModel {
	
	private Integer timer;
	private Integer minValue;
	private Integer actualValue;
	
	private Integer countSeconds;
	private Integer actualValueSeconds;
	private String soundType;
	
	
	private List<ChangeListener> listeners;	
	private boolean valueIsAdjusting;
	private int extent;
	
	public Integer getActualValueSeconds() {
		return actualValueSeconds;
	}
	public void setActualValueSeconds(Integer actualValueSeconds) {
		this.actualValueSeconds = actualValueSeconds;
	}
	public Integer getCountSeconds() {
		return countSeconds;
	}
	public void setCountSeconds(Integer countSeconds) {
		this.countSeconds = countSeconds;
	}
	public Integer getTimer() {
		return timer;
	}
	public void setTimer(Integer timer) {
		this.timer = timer;
	}
	public Integer getMinValue() {
		return minValue;
	}
	public void setMinValue(Integer minValue) {
		this.minValue = minValue;
	}
	public Integer getActualValue() {
		return actualValue;
	}
	public void setActualValue(Integer actualValue) {
		this.actualValue = actualValue;
	}
	@Override
	public int getMinimum() {
		return getMinValue();
	}
	@Override
	public void setMinimum(int newMinimum) {
		
		setMinValue(newMinimum);
		
	}
	@Override
	public int getMaximum() {
		return getTimer() * 60;
	}
	@Override
	public void setMaximum(int newMaximum) {
		setTimer(newMaximum / 60);
		
	}
	@Override
	public int getValue() {
		return getActualValueSeconds();
	}
	@Override
	public void setValue(int newValue) {
		setActualValueSeconds(newValue);
		
	}
	@Override
	public void setValueIsAdjusting(boolean b) {
		this.valueIsAdjusting = b;
		
	}
	@Override
	public boolean getValueIsAdjusting() {
		
		return valueIsAdjusting;
	}
	@Override
	public int getExtent() {
		
		return extent;
	}
	@Override
	public void setExtent(int newExtent) {
		this.extent = newExtent;
	}
	@Override
	public void setRangeProperties(int value, int extent, int min, int max, boolean adjusting) {
		this.actualValueSeconds = value;
		this.extent = extent;
		this.minValue = min;
		this.timer = max / 60;
		this.valueIsAdjusting = adjusting;
		
	}
	@Override
	public void addChangeListener(ChangeListener x) {
		if (listeners == null) {
			listeners = new ArrayList<ChangeListener>();
		}
		listeners.add(x);
		
	}
	@Override
	public void removeChangeListener(ChangeListener x) {
		if (listeners == null) {
			return;
		}
		listeners.remove(x);
		
	}
	public String getSoundType() {
		return soundType;
	}
	public void setSoundType(String soundType) {
		this.soundType = soundType;
	}
		
}
