# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Two projects on Java GUI technologies. Timer is based on a Java Swing and TimerFX is based on JavaFX 2.
* 1.0 (Timer) and 2.0 (TimerFX)

### How do I get set up? ###

* Just clone and hit "Add existing project..."  option on Eclipse

### Who do I talk to? ###

* Repo owner: Daniel N. Chaves
* https://www.linkedin.com/in/danielchaves/